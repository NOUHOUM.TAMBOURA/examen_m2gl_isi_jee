package com.isi.medecinservice.entite;

import org.springframework.data.rest.core.config.Projection;

import java.util.Date;

@Projection(name = "projectionmedecin" , types = Medecin.class)
public interface ProjectionMedecin {
   public  Long getidmedecin();
   public   String getnommedecin();
   public  String getprenommedecin();
   public    String getadressemedecin();
   public  String gettelmedecin();
   public    Date getdateNaissmedecin();


}
