package com.isi.rendezvousservice.entite;

import java.util.Date;

public class Patient {
    private long idpatient;
    private String nompatient;
    private String prenompatient;
    private String adressepatient;
    private String telpatient;
    private Date dateNaisspatient;
    private String sexepatient;

    public long getIdpatient() {
        return idpatient;
    }

    public void setIdpatient(long idpatient) {
        this.idpatient = idpatient;
    }

    public String getNompatient() {
        return nompatient;
    }

    public void setNompatient(String nompatient) {
        this.nompatient = nompatient;
    }

    public String getPrenompatient() {
        return prenompatient;
    }

    public void setPrenompatient(String prenompatient) {
        this.prenompatient = prenompatient;
    }

    public String getAdressepatient() {
        return adressepatient;
    }

    public void setAdressepatient(String adressepatient) {
        this.adressepatient = adressepatient;
    }

    public String getTelpatient() {
        return telpatient;
    }

    public void setTelpatient(String telpatient) {
        this.telpatient = telpatient;
    }

    public Date getDateNaisspatient() {
        return dateNaisspatient;
    }

    public void setDateNaisspatient(Date dateNaisspatient) {
        this.dateNaisspatient = dateNaisspatient;
    }

    public String getSexepatient() {
        return sexepatient;
    }

    public void setSexepatient(String sexepatient) {
        this.sexepatient = sexepatient;
    }

    @Override
    public String toString() {
        return "Patient{" +
                "idpatient=" + idpatient +
                ", nompatient='" + nompatient + '\'' +
                ", prenompatient='" + prenompatient + '\'' +
                ", adressepatient='" + adressepatient + '\'' +
                ", telpatient='" + telpatient + '\'' +
                ", dateNaisspatient=" + dateNaisspatient +
                ", sexepatient='" + sexepatient + '\'' +
                '}';
    }
}
