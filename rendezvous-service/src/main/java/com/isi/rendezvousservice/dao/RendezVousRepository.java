package com.isi.rendezvousservice.dao;

import com.isi.rendezvousservice.entite.RendezVous;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.Date;

@CrossOrigin("*")
@RepositoryRestResource
public interface RendezVousRepository extends JpaRepository<RendezVous, Long> {

    // chercher un medecin par nom
    @RestResource(path = "/cherchernom")
    public Page<RendezVous> findByDaterendezvousContains(@Param("date") Date date , Pageable pageable);
}
