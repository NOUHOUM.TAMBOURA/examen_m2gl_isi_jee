package com.isi.rendezvousservice.Service;

import com.isi.rendezvousservice.dao.MedecinService;
import com.isi.rendezvousservice.dao.PatientService;
import com.isi.rendezvousservice.dao.RendezVousRepository;
import com.isi.rendezvousservice.entite.RendezVous;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin("*")
@RestController
public class DetailsRendezVous {
    @Autowired
    private MedecinService medecinService;
    @Autowired
    private PatientService patientService;
    @Autowired
    private RendezVousRepository rendezVousRepository;

    // une methode qui return les details sur un Rendez-Vous


    @GetMapping("/detailsRendezVous/{id}")
    public RendezVous getRendezVous(@PathVariable(name = "id") Long id){
        RendezVous rdv1= rendezVousRepository.findById(id).get();
        rdv1.setPatient(patientService.findPatientById(rdv1.getIdpatient()));
        rdv1.setMedecin(medecinService.findMedecinById(rdv1.getIdmedecin()));
        return  rdv1;
    }
}
