package com.isi.consultationservice.entite;

import java.util.Date;

public class RendezVous {
    private long idrendezvous;
    private Date daterendezvous;

    private Long idpatient;
    private Long idmedecin;

    public long getIdrendezvous() {
        return idrendezvous;
    }

    public void setIdrendezvous(long idrendezvous) {
        this.idrendezvous = idrendezvous;
    }

    public Date getDaterendezvous() {
        return daterendezvous;
    }

    public void setDaterendezvous(Date daterendezvous) {
        this.daterendezvous = daterendezvous;
    }

    public Long getIdpatient() {
        return idpatient;
    }

    public void setIdpatient(Long idpatient) {
        this.idpatient = idpatient;
    }

    public Long getIdmedecin() {
        return idmedecin;
    }

    public void setIdmedecin(Long idmedecin) {
        this.idmedecin = idmedecin;
    }

    @Override
    public String toString() {
        return "RendezVous{" +
                "idrendezvous=" + idrendezvous +
                ", daterendezvous=" + daterendezvous +
                ", idpatient=" + idpatient +
                ", idmedecin=" + idmedecin +
                '}';
    }
}
