package com.isi.consultationservice.entite;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Consultation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idconsultation;
    private Date dateconsultation;
    //@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Long idpatient;
    //@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Long idrendezvous;

    @Transient
    private Patient patient;
    @Transient
    private RendezVous rendezVous;

    public Consultation() {
    }

    public Consultation(Date dateconsultation, Long idpatient, Long idrendezvous) {
        this.dateconsultation = dateconsultation;
        this.idpatient = idpatient;
        this.idrendezvous = idrendezvous;
    }



    public Consultation(Date dateconsultation, Long idpatient, Long idrendezvous, Patient patient, RendezVous rendezVous) {
        this.dateconsultation = dateconsultation;
        this.idpatient = idpatient;
        this.idrendezvous = idrendezvous;
        this.patient = patient;
        this.rendezVous = rendezVous;
    }



    public long getIdconsultation() {
        return idconsultation;
    }

    public void setIdconsultation(long idconsultation) {
        this.idconsultation = idconsultation;
    }

    public Date getDateconsultation() {
        return dateconsultation;
    }

    public void setDateconsultation(Date dateconsultation) {
        this.dateconsultation = dateconsultation;
    }

    public Long getIdpatient() {
        return idpatient;
    }

    public void setIdpatient(Long idpatient) {
        this.idpatient = idpatient;
    }

    public Long getIdrendezvous() {
        return idrendezvous;
    }

    public void setIdrendezvous(Long idrendezvous) {
        this.idrendezvous = idrendezvous;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public RendezVous getRendezVous() {
        return rendezVous;
    }

    public void setRendezVous(RendezVous rendezVous) {
        this.rendezVous = rendezVous;
    }

    @Override
    public String toString() {
        return "Consultation{" +
                "idconsultation=" + idconsultation +
                ", dateconsultation=" + dateconsultation +
                ", idpatient=" + idpatient +
                ", idrendezvous=" + idrendezvous +
                ", patient=" + patient +
                ", rendezVous=" + rendezVous +
                '}';
    }
}
