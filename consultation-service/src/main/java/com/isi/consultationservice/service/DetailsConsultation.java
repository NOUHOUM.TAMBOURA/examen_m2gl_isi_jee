package com.isi.consultationservice.service;

import com.isi.consultationservice.dao.ConsultationRepository;
import com.isi.consultationservice.dao.PatientService;
import com.isi.consultationservice.dao.RendezVousService;
import com.isi.consultationservice.entite.Consultation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
public class DetailsConsultation {

    @Autowired
    private RendezVousService rendezVousService;
    @Autowired
    private PatientService patientService;
    @Autowired
    private ConsultationRepository consultationRepository;

    @GetMapping("/detailsconsultations/{id}")
    public Consultation getConsultation(@PathVariable(name = "id") Long id){
        Consultation consultation= consultationRepository.findById(id).get();
        consultation.setPatient(patientService.findPatientById(consultation.getIdpatient()));
        consultation.setRendezVous(rendezVousService.findRendezVousById(consultation.getIdrendezvous()));
        return consultation;

    }

}
