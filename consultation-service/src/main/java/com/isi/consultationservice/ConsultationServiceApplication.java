package com.isi.consultationservice;

import com.isi.consultationservice.dao.ConsultationRepository;
import com.isi.consultationservice.dao.PatientService;
import com.isi.consultationservice.dao.RendezVousService;
import com.isi.consultationservice.entite.Consultation;
import com.isi.consultationservice.entite.Patient;
import com.isi.consultationservice.entite.RendezVous;
import com.isi.consultationservice.service.DetailsConsultation;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.hateoas.PagedModel;

import java.util.Date;

@SpringBootApplication
@EnableFeignClients
public class ConsultationServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConsultationServiceApplication.class, args);
	}

	@Bean
	CommandLineRunner runner(ConsultationRepository consultationRepository ,
							 RepositoryRestConfiguration repositoryRestConfiguration,
							 PatientService patientService, RendezVousService rendezVousService,
							 DetailsConsultation detailsConsultation){
		return args -> {
			//=============================================================================================
			PagedModel<Patient> patient = patientService.findAllPatient();
			patient.getContent().forEach(p ->{
				PagedModel<RendezVous> rendezVous = rendezVousService.findAllRendezVous();
				rendezVous.getContent().forEach(rdv ->{

					consultationRepository.save(new Consultation(new Date(),p.getIdpatient(),rdv.getIdrendezvous()));
					repositoryRestConfiguration.exposeIdsFor(Consultation.class);

				});

			});
		};
	}
}
